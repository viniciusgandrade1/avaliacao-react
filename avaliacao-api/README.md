# Informçaões úteis

#Figma
[clica aqui](https://www.figma.com/file/01DbXPmZizS0r1AUQaEu56/Set-Off?node-id=1%3A2)

## frontend
```
$  ionic info
  
  Ionic:
  
     Ionic CLI                     : 5.4.16 (/Users/pauloborges/.nvm/versions/node/v12.16.1/lib/node_modules/ionic)
     Ionic Framework               : @ionic/angular 5.0.7
     @angular-devkit/build-angular : 0.803.29
     @angular-devkit/schematics    : 9.0.7
     @angular/cli                  : 8.3.29
     @ionic/angular-toolkit        : 2.1.2
  
  Utility:
  
     cordova-res : not installed
     native-run  : not installed
  
  System:
  
     NodeJS : v12.16.1 (/Users/pauloborges/.nvm/versions/node/v12.16.1/bin/node)
     npm    : 6.13.4
     OS     : macOS Catalina

```

## Backend
```
$ java -version
java version "1.8.0_192"
Java(TM) SE Runtime Environment (build 1.8.0_192-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.192-b12, mixed mode)

```

## Docker

ver arquivos na pasta "docker"

# Rodar docker

```` $ cd docker && docker-compose up -d ````


# CI/CD
ver arquivo .gitlab-ci.yml
