package br.com.avaliacao.modelo;

import br.com.avaliacao.modelo.converter.CpfConverter;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_cliente", schema = "public")
public class    Cliente {
    @Id
    @Column(name = "id_cliente", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCliente;

    @Column(name = "nome", nullable = false, length = 100)
    @NotEmpty(message = "Nome é obrigatório")
    private String nome;

    @Column(name = "cpf", nullable = false, length = 11)
    @NotEmpty(message = "CPF é obrigatório")
    @Convert(converter = CpfConverter.class)
    private String cpf;

    @ManyToOne()
    @JoinColumn(name = "id_endereco")
    private Endereco endereco;

    @OneToMany(targetEntity = Email.class, mappedBy = "cliente", cascade = CascadeType.REFRESH)
    @JsonManagedReference
    List<Email> emails;

    @OneToMany(targetEntity = Telefone.class, mappedBy = "cliente", cascade = CascadeType.REFRESH)
    @JsonManagedReference
    List<Telefone> telefones;

    public void addEmail(Email email){
        //As said Hibernate will ignore it when persist this relationship.
        //Add it mainly for the consistency of this relationship for both side in the Java instance
        this.emails.add(email);
        email.setCliente(this);
    }

    public void addTelefone(Telefone telefone){
        //As said Hibernate will ignore it when persist this relationship.
        //Add it mainly for the consistency of this relationship for both side in the Java instance
        this.telefones.add(telefone);
        telefone.setCliente(this);
    }
}
