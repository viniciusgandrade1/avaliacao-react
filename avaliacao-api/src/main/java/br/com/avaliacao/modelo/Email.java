package br.com.avaliacao.modelo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_email", schema = "public")
public class Email {

    @Id
    @Column(name = "id_email", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEmail;

    @Column(name = "ds_email", nullable = false, length = 100)
    @NotEmpty(message = "e-mail")
    private String dsEmail;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_cliente")
    @JsonBackReference
    Cliente cliente;
}
