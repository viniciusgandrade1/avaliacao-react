package br.com.avaliacao.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_endereco", schema = "public")
public class Endereco {

    @Id
    @Column(name = "id_endereco", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEndereco;

    @Column(name = "cep", nullable = false, length = 100)
    @NotEmpty(message = "CEP é obrigatório")
    private String cep;

    @Column(name = "logradouro", nullable = false, length = 100)
    @NotEmpty(message = "Logradouro é obrigatório")
    private String logradouro;

    @Column(name = "bairro", nullable = false, length = 100)
    @NotEmpty(message = "Bairro é obrigatório")
    private String bairro;

    @Column(name = "cidade", nullable = false, length = 100)
    @NotEmpty(message = "Cidade é obrigatória")
    private String cidade;

    @Column(name = "uf", nullable = false, length = 100)
    @NotEmpty(message = "UF é obrigatória")
    private String uf;

    @Column(name = "complemento")
    private String complemento;
}
