package br.com.avaliacao.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_perfil", schema = "public")
public class Perfil implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_perfil", nullable = false)
    private Long idPerfil;

    @Column(name = "role", nullable = false, length = 100)
    @NotEmpty(message = "perfil name")
    @Enumerated(EnumType.STRING)
    private UserRoleName role;

    public String getAuthority() {
        return role.name();
    }

    public void setRole(UserRoleName name) {
        this.role = name;
    }

    @JsonIgnore
    public UserRoleName getRole() {
        return role;
    }
}
