package br.com.avaliacao.modelo;
import br.com.avaliacao.modelo.converter.TelefoneConverter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_telefone", schema = "public")
public class Telefone {

    @Id
    @Column(name = "id_telefone", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTelefone;

    @Column(name = "numero", nullable = false, length = 9)
    @NotEmpty(message = "Número é obrigatório")
    @Convert(converter = TelefoneConverter.class)
    private String numero;

    @Column(name = "tipo", nullable = false, length = 11)
    @NotNull(message = "Tipo é obrigatório")
    private Integer tipo;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_cliente")
    @JsonBackReference
    Cliente cliente;
}
