package br.com.avaliacao.modelo.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.swing.text.MaskFormatter;
import java.text.ParseException;

@Converter
public class CpfConverter implements AttributeConverter<String, String> {
    @Override
    public String convertToDatabaseColumn(String cpf) {
        return cpf.replaceAll("[^0-9]+", "");
    }

    @Override
    public String convertToEntityAttribute(String cpf) {
        try{
            MaskFormatter format = new MaskFormatter("###.###.###-##");
            format.setValueContainsLiteralCharacters(false);
            return format.valueToString(cpf);
        } catch (ParseException e) {
            return "";
        }
    }
}
