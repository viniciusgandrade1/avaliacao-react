package br.com.avaliacao.modelo.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.swing.text.MaskFormatter;
import java.text.ParseException;

@Converter
public class TelefoneConverter implements AttributeConverter<String, String> {
    @Override
    public String convertToDatabaseColumn(String telefone) {
        return telefone.replaceAll("[^0-9]+", "");
    }

    @Override
    public String convertToEntityAttribute(String telefone) {
        try {
            MaskFormatter format = null;
            if (telefone.length() == 8) {
                format = new MaskFormatter("####-####");
            } else {
                format = new MaskFormatter("#####-####");
            }
            format.setValueContainsLiteralCharacters(false);
            return format.valueToString(telefone);
        } catch (ParseException e) {
            return "";
        }
    }
}
