package br.com.avaliacao.repositorio;

import br.com.avaliacao.modelo.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmailRepository extends JpaRepository<Email, Long> {
    @Query(value = "select e " +
            "  from Email e " +
            " where lower(e.cliente.idCliente) = ?1 ")
    List<Email> findByIdCliente(Long idCliente);

    @Modifying
    @Query("delete from Email u where u.cliente.idCliente = ?1")
    void deleteEmailWithClientId(Long idClient);
}
