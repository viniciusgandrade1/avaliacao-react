package br.com.avaliacao.repositorio;

import br.com.avaliacao.modelo.Telefone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TelefoneRepository extends JpaRepository<Telefone, Long> {
    @Modifying
    @Query("delete from Telefone u where u.cliente.idCliente = ?1")
    void deleteTelefoneWithClientId(Long idClient);
}
