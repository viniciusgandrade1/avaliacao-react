package br.com.avaliacao.resource;

import com.google.common.base.Splitter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Base64;
import java.util.List;

public class AbstractResource {

    protected HttpHeaders getHttpHeaders(String filename, String applicationValue) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(applicationValue));
        headers.add("content-disposition", "inline;filename=" + filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        return headers;
    }

    /**
     * Faz o decode de uma string base64
     *
     * @param params Valores agrupados
     * @return Lista de valores
     */
    protected List<String> decodeBase64Params(String params, String separador) {
        byte[] decodedBytes = Base64.getDecoder().decode(params);
        String decodedString = new String(decodedBytes);
        return Splitter.on(separador).trimResults().omitEmptyStrings().splitToList(decodedString);
    }
}
