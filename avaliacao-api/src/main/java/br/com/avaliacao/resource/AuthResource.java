package br.com.avaliacao.resource;

import br.com.avaliacao.exception.ServiceException;
import br.com.avaliacao.modelo.UserTokenState;
import br.com.avaliacao.modelo.Usuario;
import br.com.avaliacao.security.TokenHelper;
import br.com.avaliacao.security.auth.JwtAuthenticationRequest;
import br.com.avaliacao.util.EntityConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@RestController
@CrossOrigin
@RequestMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthResource {

    @Autowired
    TokenHelper tokenHelper;

    @Lazy
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    EntityConverter entityConverter;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(
            @RequestBody JwtAuthenticationRequest authenticationRequest,
            HttpServletResponse response
    ) {
        try {
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getUsername(),
                    authenticationRequest.getPassword()
            );
            // Perform the security
            final Authentication authentication = authenticationManager.authenticate(auth);

            // Inject into security context
            SecurityContextHolder.getContext().setAuthentication(authentication);

            // token creation
            Usuario usuario = (Usuario) authentication.getPrincipal();

            usuario.setAccess_token(tokenHelper.generateToken(usuario.getUsername()));
            usuario.setExpires_in(tokenHelper.getExpiredIn());
            usuario.setPassword("");
            return ResponseEntity.ok(usuario);
        } catch (InternalAuthenticationServiceException e) {
            throw new ServiceException(e.getMessage());
        } catch (BadCredentialsException e) {
            throw new ServiceException("Usuário e/ou senha inválidos.");
        } catch (Exception e) {
            throw new ServiceException("Não foi possível realizar o login. Por favor tente mais tarde");
        }
    }

    @RequestMapping(value = "/refresh", method = RequestMethod.POST)
    public ResponseEntity<?> refreshAuthenticationToken(
            HttpServletRequest request,
            HttpServletResponse response,
            Principal principal
    ) {

        String authToken = tokenHelper.getToken(request);

        if (authToken != null && principal != null) {

            String refreshedToken = tokenHelper.refreshToken(authToken);
            int expiresIn = tokenHelper.getExpiredIn();

            return ResponseEntity.ok(new UserTokenState(refreshedToken, expiresIn));
        } else {
            UserTokenState userTokenState = new UserTokenState();
            return ResponseEntity.accepted().body(userTokenState);
        }
    }
}
