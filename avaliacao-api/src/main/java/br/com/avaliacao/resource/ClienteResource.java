package br.com.avaliacao.resource;

import br.com.avaliacao.builder.Resposta;
import br.com.avaliacao.builder.RespostaBuilder;
import br.com.avaliacao.modelo.Cliente;
import br.com.avaliacao.modelo.Endereco;
import br.com.avaliacao.service.ClienteService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/cliente", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ClienteResource extends AbstractResource {

    @Autowired
    private ClienteService clienteService;

    @ApiOperation(
            value = "Recupera os dados de um cliente"
    )
    @GetMapping(value = "/{id}")
    @ResponseBody
    public Cliente get(@PathVariable("id") Long id) {
        return this.clienteService.obter(id);
    }

    @ApiOperation(
            value = "Recupera todos os clientes"
    )
    @GetMapping
    @ResponseBody
    public List<Cliente> listar() {
        return this.clienteService.listar();
    }

    @ApiOperation(
            value = "Cria um cliente"
    )
    @PostMapping
    @ResponseBody
    public Cliente post(@RequestBody Cliente cliente) {
        return this.clienteService.criar(cliente);
    }

    @PutMapping("/{id:[0-9]+}")
    @ApiOperation("Atualiza um cliente")
    @CrossOrigin
    public Cliente update(
            @ApiParam(name = "id", value = "cliente id")
            @PathVariable("id") Long id,
            @Valid @RequestBody Cliente cliente) {
        cliente.setIdCliente(id);
        return this.clienteService.atualizar(cliente);
    }

    @ApiOperation(
            value = "Excluir um cliente"
    )
    @DeleteMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity delete(@PathVariable("id") Long id) {
        this.clienteService.remover(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
