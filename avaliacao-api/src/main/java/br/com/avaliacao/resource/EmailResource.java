package br.com.avaliacao.resource;

import br.com.avaliacao.modelo.Email;
import br.com.avaliacao.repositorio.EmailRepository;
import br.com.avaliacao.service.EmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;

@RestController("email-resource")
@RequestMapping("/email")
@Api(tags = "email")
public class EmailResource {

    private final EmailRepository emailRepository;
    private final EmailService emailService;

    public EmailResource(
            EmailRepository emailRepository,
            EmailService emailService
    ) {
        this.emailRepository = emailRepository;
        this.emailService = emailService;
    }

    @PostMapping("")
    @CrossOrigin
    @Transactional
    @ApiOperation("Register of an email")
    public Email criar(@Valid @RequestBody Email email) {
        return this.emailService.criar(email);
    }

    @PutMapping("/{id:[0-9]+}")
    @ApiOperation("Updates email by id")
    @CrossOrigin
    public Email atualizar(
            @ApiParam(name = "id", value = "email id")
            @PathVariable("id") Long id,
            @Valid @RequestBody Email email) {
        return this.emailService.save(email, id);
    }

    @DeleteMapping("/{id:[0-9]+}")
    @ApiOperation("Deletes an email by id")
    @CrossOrigin
    @Transactional
    public ResponseEntity<Email> remover(
            @ApiParam(name = "id", value = "email id")
            @PathVariable("id") Long id) {
        this.emailRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK)
                .build();
    }

    @GetMapping("")
    @ApiParam("List of emails")
    @CrossOrigin
    public Page<Email> listar() {
        return this.emailRepository.findAll(PageRequest.of(0, 9999));
    }

    @GetMapping("/{id:[0-9]+}")
    @CrossOrigin
    @ApiOperation("Retrieve email by id")
    public Email obter(
            @ApiParam(name = "id", value = "email id", required = true)
            @PathVariable("id") Long id
    ) {
        return this.emailRepository.findById(id).get();
    }
}
