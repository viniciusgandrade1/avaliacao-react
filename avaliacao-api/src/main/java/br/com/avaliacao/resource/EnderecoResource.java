package br.com.avaliacao.resource;

import br.com.avaliacao.builder.Resposta;
import br.com.avaliacao.builder.RespostaBuilder;
import br.com.avaliacao.modelo.Cep;
import br.com.avaliacao.modelo.Endereco;
import br.com.avaliacao.repositorio.EnderecoRepository;
import br.com.avaliacao.service.EnderecoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;

@RestController("endereco-resource")
@RequestMapping("/endereco")
@Api(tags = "endereco")
public class EnderecoResource {

    private final EnderecoRepository enderecoRepository;
    private final EnderecoService enderecoService;

    public EnderecoResource(
            EnderecoRepository enderecoRepository,
            EnderecoService enderecoService
    ) {
        this.enderecoRepository = enderecoRepository;
        this.enderecoService = enderecoService;
    }

    @PostMapping("")
    @CrossOrigin
    @Transactional
    @ApiOperation("Registra um endereco")
    public Endereco create(@Valid @RequestBody Endereco endereco) {
        return this.enderecoService.salvar(endereco);
    }

    @PutMapping("/{id:[0-9]+}")
    @ApiOperation("Atualiza um endereco")
    @CrossOrigin
    public Endereco update(
            @ApiParam(name = "id", value = "endereco id")
            @PathVariable("id") Long id,
            @Valid @RequestBody Endereco endereco) {
        return this.enderecoService.atualizar(endereco, id);
    }

    @DeleteMapping("/{id:[0-9]+}")
    @ApiOperation("Remove um endereco")
    @CrossOrigin
    @Transactional
    public ResponseEntity delete(
            @ApiParam(name = "id", value = "endereco id")
            @PathVariable("id") Long id) {
        this.enderecoRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/cep/{cep:[0-9]+}")
    @CrossOrigin
    @ApiOperation("Consulta um CEP")
    public Cep retrieveCep(
            @ApiParam(name = "cep", value = "cep", required = true)
            @PathVariable("cep") String cep
    ) {
        return this.enderecoService.getCep(cep);
    }
}
