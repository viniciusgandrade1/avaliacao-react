package br.com.avaliacao.resource;

import br.com.avaliacao.modelo.Telefone;
import br.com.avaliacao.repositorio.TelefoneRepository;
import br.com.avaliacao.service.TelefoneService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;

@RestController("telefone-resource")
@RequestMapping("/telefone")
@Api(tags = "telefone")
public class TelefoneResource {

    private final TelefoneRepository telefoneRepository;
    private final TelefoneService telefoneService;

    public TelefoneResource(
            TelefoneRepository telefoneRepository,
            TelefoneService telefoneService
    ) {
        this.telefoneRepository = telefoneRepository;
        this.telefoneService = telefoneService;
    }

    @PostMapping("")
    @CrossOrigin
    @Transactional
    @ApiOperation("Register of a telefone")
    public Telefone criar(@Valid @RequestBody Telefone telefone) {
        return this.telefoneService.criar(telefone);
    }

    @PutMapping("/{id:[0-9]+}")
    @ApiOperation("Updates telefone by id")
    @CrossOrigin
    public Telefone atualizar(
            @ApiParam(name = "id", value = "telefone id")
            @PathVariable("id") Long id,
            @Valid @RequestBody Telefone telefone) {
        return this.telefoneService.save(telefone, id);
    }

    @DeleteMapping("/{id:[0-9]+}")
    @ApiOperation("Deletes a telefone by id")
    @CrossOrigin
    @Transactional
    public ResponseEntity<Telefone> remover(
            @ApiParam(name = "id", value = "telefone id")
            @PathVariable("id") Long id) {
        this.telefoneRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("")
    @ApiParam("List of telefones")
    @CrossOrigin
    public Page<Telefone> listar() {
        return this.telefoneRepository.findAll(PageRequest.of(0, 99999));
    }

    @GetMapping("/{id:[0-9]+}")
    @CrossOrigin
    @ApiOperation("Retrieve telefone by id")
    public Telefone obter(
            @ApiParam(name = "id", value = "telefone id", required = true)
            @PathVariable("id") Long id
    ) {
        return this.telefoneRepository.findById(id).get();
    }
}
