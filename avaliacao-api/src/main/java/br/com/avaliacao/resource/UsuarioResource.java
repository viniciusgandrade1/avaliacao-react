package br.com.avaliacao.resource;

import br.com.avaliacao.builder.Resposta;
import br.com.avaliacao.builder.RespostaBuilder;
import br.com.avaliacao.modelo.Usuario;
import br.com.avaliacao.service.UsuarioService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/usuario", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class UsuarioResource extends AbstractResource {

    @Autowired
    private UsuarioService usuarioService;

    @ApiOperation(
            value = "Criar ou atualiza usuário"
    )
    @PostMapping
    @ResponseBody
    public Usuario postUsuario(@RequestBody Usuario usuario) {
        return this.usuarioService.create(usuario);
    }
}
