package br.com.avaliacao.resource;

import br.com.avaliacao.builder.Resposta;
import br.com.avaliacao.builder.RespostaBuilder;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class VersaoResource extends AbstractResource {

    @Value("${api-version:#{null}}")
    private String apiVersion;

    @GetMapping
    @ResponseBody
    public ResponseEntity<Resposta> getVersao() {
        JSONObject versao = new JSONObject();
        versao.put("versao", this.apiVersion == null? "1.0.0" : this.apiVersion);
        versao.put("webservice", "Avaliação");
        Resposta resposta = RespostaBuilder.getBuilder().resposta(versao).build();
        return new ResponseEntity<>(resposta, HttpStatus.OK);
    }

}
