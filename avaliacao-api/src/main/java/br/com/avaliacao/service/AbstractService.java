package br.com.avaliacao.service;

import br.com.avaliacao.modelo.Usuario;
import br.com.avaliacao.repositorio.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class AbstractService {

    @Autowired
    UsuarioRepository usuarioRepository;

    /**
     * Salva o arquivo em disco no storage
     *
     * @param id   identificador do arquivo - sera o nome dentro do storage
     * @param file arquivo a ser gravado
     * @param path caminho onde sera gravado
     * @throws IOException
     */
    protected void saveFileInDisc(Long id, MultipartFile file, String path, String extension) throws IOException {
        FileOutputStream fos = this.initCriarArquivo(id, path, extension);
        fos.write(file.getBytes());
        fos.close();
    }

    protected void saveBase64InDisc(Long id, String image, String path, String extension) {
        try {
            byte[] imageByte = Base64.getDecoder().decode(image);
            FileOutputStream fos = this.initCriarArquivo(id, path, extension);
            fos.write(imageByte);
            fos.close();
        } catch (IOException e) {
            System.out.println(e);
            throw new RuntimeException("Não foi possível salvar a imagem em disco");
        }
    }

    private FileOutputStream initCriarArquivo(Long id, String path, String extension) throws FileNotFoundException {
        File fileImage = new File(path);
        if (!fileImage.exists()) {
            if (!fileImage.mkdirs()) {
                throw new RuntimeException("Não foi possível criar diretorio.");
            }
        }
        // salva pdf no storage
        return new FileOutputStream(
                new File(path + id + extension)
        );
    }

    /**
     * Remover aquivo do storage
     *
     * @param fileFullPath
     * @throws IOException
     */
    public void removeFileInDisc(String fileFullPath) throws IOException {
        File file = new File(fileFullPath);
        if (file.exists()) {
            if (file.delete()) {
                return;
            }
            throw new IOException("Não foi possível remover o arquivo.");
        }
    }

    /**
     * Retorna se existe arquivo
     *
     * @return boolean
     */
    public boolean fileExists(String filePath, String fileName) {
        File fPath = new File(filePath);
        if (!fPath.exists()) {
            if (!fPath.mkdirs()) {
                throw new RuntimeException("Não foi possível criar diretorio.");
            }
        }
        File file = new File(filePath + fileName);
        boolean exists = file.exists();
        return exists;
    }

    public MultiValueMap<String, String> getStringStringMultiValueMapFormdata(String authorization) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "multipart/form-data");
        map.put("Authorization", authorization);
        headers.setAll(map);
        return headers;
    }

    public MultiValueMap<String, String> getStringStringMultiValueMap(String token) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", "Bearer " + token);
        headers.setAll(map);
        return headers;
    }

    public MultiValueMap<String, String> getStringMultiValueMapNoAuth() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        headers.setAll(map);
        return headers;
    }

    protected Usuario getUsuarioAutenticado(Authentication authentication) throws RuntimeException {
        Usuario authUser = (Usuario) authentication.getPrincipal();
        Usuario usuario = this.usuarioRepository.findByUsername(authUser.getUsername());
        if (usuario == null) {
            throw new RuntimeException("Usuário não econtrado");
        }
        return usuario;
    }
}
