package br.com.avaliacao.service;

import br.com.avaliacao.modelo.Cliente;
import br.com.avaliacao.modelo.Email;
import br.com.avaliacao.repositorio.*;
import br.com.avaliacao.exception.ServiceException;
import br.com.avaliacao.util.EntityConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService extends AbstractService {

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    EmailRepository emailRepository;

    @Autowired
    TelefoneRepository telefoneRepository;

    @Autowired
    EnderecoRepository enderecoRepository;

    @Autowired
    EntityConverter entityConverter;

    public Cliente obter(Long id) {
       return this.clienteRepository.findById(id).get();
    }

    public List<Cliente> listar() {
        return this.clienteRepository.findAll();
    }

    @Transactional
    public Cliente criar(Cliente cliente) {
        Cliente clienteCadastrado = this.clienteRepository.findByCpf(cliente.getCpf());
        if (clienteCadastrado != null) {
            throw new ServiceException("Já existe um cliente para este CPF");
        }
        return this.clienteRepository.save(cliente);
    }

    @Transactional
    public Cliente atualizar(Cliente cliente) {
        Optional<Cliente> clienteCadastrado = this.clienteRepository.findById(cliente.getIdCliente());
        if (clienteCadastrado == null || !clienteCadastrado.isPresent()) {
            throw new ServiceException("Cliente não encontrado");
        }
        cliente.setIdCliente(cliente.getIdCliente());
        return this.clienteRepository.save(cliente);
    }

    @Transactional
    public void remover(Long id) {
        Optional<Cliente> cli = this.clienteRepository.findById(id);
        if (cli == null || !cli.isPresent()) {
            throw new ServiceException("Cliente não encontrado");
        }
        Cliente cliente = cli.get();
        this.emailRepository.deleteEmailWithClientId(cliente.getIdCliente());
        this.telefoneRepository.deleteTelefoneWithClientId(cliente.getIdCliente());
        this.clienteRepository.deleteById(cliente.getIdCliente());
        this.enderecoRepository.deleteById(cliente.getEndereco().getIdEndereco());
    }
}
