package br.com.avaliacao.service;

import br.com.avaliacao.modelo.Email;
import br.com.avaliacao.repositorio.ClienteRepository;
import br.com.avaliacao.repositorio.EmailRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class EmailService {

    private final EmailRepository emailRepository;
    private final ClienteRepository clienteRepository;

    public EmailService(EmailRepository emailRepository, ClienteRepository clienteRepository) {
        this.emailRepository = emailRepository;
        this.clienteRepository = clienteRepository;
    }

    @Transactional
    public Email criar(Email email) {
        if (email.getIdEmail() != null) {
            Email registeredEmail = this.emailRepository.findById(email.getIdEmail()).get();
            if (registeredEmail != null) return registeredEmail;
        }
        email.setCliente(clienteRepository.findById(email.getCliente().getIdCliente()).get());
        email.getCliente().addEmail(email);
        return this.emailRepository.save(email);
    }

    @Transactional
    public Email save(Email email, Long id) {
        email.setIdEmail(id);
        email.setCliente(clienteRepository.findById(email.getCliente().getIdCliente()).get());
        return this.emailRepository.save(email);
    }
}
