package br.com.avaliacao.service;

import br.com.avaliacao.modelo.Cep;
import br.com.avaliacao.modelo.Endereco;
import br.com.avaliacao.repositorio.EnderecoRepository;
import com.google.gson.Gson;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class EnderecoService {
    private final EnderecoRepository enderecoRepository;

    public EnderecoService(EnderecoRepository enderecoRepository) {
        this.enderecoRepository = enderecoRepository;
    }

    @Transactional
    public Endereco salvar(Endereco endereco) {
        return this.enderecoRepository.save(endereco);
    }

    @Transactional
    public Endereco atualizar(Endereco endereco, Long id) {
        endereco.setIdEndereco(id);
        return this.enderecoRepository.save(endereco);
    }

    public Cep getCep(String cep) {
        String urlGet = "http://viacep.com.br/ws/" + cep + "/json";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = null;
        Gson gson = new Gson();

        response = restTemplate.getForEntity(urlGet, String.class);

        return gson.fromJson(response.getBody(), Cep.class);
    }
}
