package br.com.avaliacao.service;

import br.com.avaliacao.modelo.Cliente;
import br.com.avaliacao.modelo.Telefone;
import br.com.avaliacao.repositorio.ClienteRepository;
import br.com.avaliacao.repositorio.TelefoneRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class TelefoneService {

    private final TelefoneRepository telefoneRepository;
    private final ClienteRepository clienteRepository;

    public TelefoneService(TelefoneRepository telefoneRepository, ClienteRepository clienteRepository) {
        this.telefoneRepository = telefoneRepository;
        this.clienteRepository = clienteRepository;
    }

    @Transactional
    public Telefone criar(Telefone telefone) {
        if (telefone.getIdTelefone() != null) {
            Telefone telCadastrado = this.telefoneRepository.findById(telefone.getIdTelefone()).get();
            if (telCadastrado != null) return telCadastrado;
        }
        telefone.setCliente(clienteRepository.findById(telefone.getCliente().getIdCliente()).get());
        return this.telefoneRepository.save(telefone);
    }

    @Transactional
    public Telefone save(Telefone telefone, Long id) {
        telefone.setIdTelefone(id);
        return this.telefoneRepository.save(telefone);
    }
}
