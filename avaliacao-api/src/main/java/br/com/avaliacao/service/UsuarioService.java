package br.com.avaliacao.service;

import br.com.avaliacao.exception.ServiceException;
import br.com.avaliacao.modelo.Usuario;
import br.com.avaliacao.repositorio.PerfilRepository;
import br.com.avaliacao.repositorio.UsuarioRepository;
import br.com.avaliacao.util.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService extends AbstractService {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UsuarioService(@Lazy PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    PerfilRepository perfilRepository;

    public Usuario create(Usuario usuario) {
        String novaSenha = this.gerarNovaSenha(usuario);
        this.usuarioRepository.save(usuario);
        usuario.setPasswordOpen(novaSenha);
        return usuario;
    }

    private String gerarNovaSenha(Usuario u) {
        String novaSenha = "123456";
        u.setPassword(this.passwordEncoder.encode(novaSenha));
        this.usuarioRepository.save(u);
        return novaSenha;
    }
}
