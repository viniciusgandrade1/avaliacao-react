package br.com.avaliacao.service.impl;

import br.com.avaliacao.modelo.Usuario;

import java.util.List;

public interface UserService {

    Usuario findById(Long id);

    Usuario findByUsername(String username);

    List<Usuario> findAll();
}
