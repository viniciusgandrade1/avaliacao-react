package br.com.avaliacao.service.impl;

import br.com.avaliacao.modelo.Usuario;
import br.com.avaliacao.repositorio.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UsuarioRepository userRepository;

    @Override
    public Usuario findByUsername(String username ) throws UsernameNotFoundException {
        Usuario u = userRepository.findByUsername( username );
        return u;
    }

    public Usuario findById( Long id ) throws AccessDeniedException {
        Optional<Usuario> u = userRepository.findById( id );
        return u.get();
    }

    public List<Usuario> findAll() throws AccessDeniedException {
        List<Usuario> result = userRepository.findAll();
        return result;
    }
}
