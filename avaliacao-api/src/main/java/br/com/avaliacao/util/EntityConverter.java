package br.com.avaliacao.util;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.lang.reflect.Type;

@Component
public class EntityConverter implements Serializable {
    private ModelMapper map;

    @PostConstruct
    private void init() {
        map = new ModelMapper();
    }

    public <D> D converter(Object source, Class<D> target){
        D retorno = map.map(source, target);

        return retorno;
    }

    public <D> D converterStrict(Object source, Class<D> target){

        if (source == null){
            return null;
        }

        map.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        D retorno = map.map(source, target);

        return retorno;
    }

    public <D> D converterStrict(Object source, Type target){
        map.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return  map.map(source, target);

    }

    public <D> D converterLazyLoading(Object source, Class<D> target){
        return map.map(source, target);
    }

    public <D> D converterStrictLazyLoading(Object source, Class<D> target){
        map.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        D retorno = map.map(source, target);

        return retorno;
    }

    public <D> D converterListaStrictLazyLoading(Object source, Type target) {
        map.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return map.map(source, target);
    }
}
