package br.com.avaliacao.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtil {
    public static void gravarDocumentoStorage(MultipartFile file, String path, Long idDocumento) throws IOException {
        File fileImage = new File(path);
        if (!fileImage.exists()) {
            if (!fileImage.mkdirs()) {
                throw new RuntimeException("Não foi possível criar diretorio para salvar documento");
            }
        }
        // salva pdf no storage
        FileOutputStream fos = new FileOutputStream(
                new File(path + "/" + idDocumento + ".pdf")
        );
        fos.write(file.getBytes());
        fos.close();
    }
}
