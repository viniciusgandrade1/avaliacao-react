package br.com.avaliacao.util;

import org.springframework.core.io.ClassPathResource;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class GeneralUtil {

    private BufferedImage getOverly() throws IOException {
        ClassPathResource cpr = new ClassPathResource("img/logo.png");
        return  ImageIO.read(cpr.getInputStream());
    }

    public String getValueObject(Object o) {
        if (o != null) {
            return o.toString();
        }
        return "";
    }

    public static String getBaseUrlFromRequest(HttpServletRequest request) {
        StringBuffer url = request.getRequestURL();
        String uri = request.getRequestURI();
        String ctx = request.getContextPath();
        return url.substring(0, url.length() - uri.length() + ctx.length()) + "/";
    }
}
