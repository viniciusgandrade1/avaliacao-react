package br.com.avaliacao.util;

import javax.swing.text.MaskFormatter;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class MaskUtil {
    public static String getCpfWithMask(String nuCpf) {
        try {
            if (nuCpf == null || nuCpf.trim().length() == 0) {
                return "";
            }
            MaskFormatter cpfFormat = new javax.swing.text.MaskFormatter("###.###.###-##");
            cpfFormat.setValueContainsLiteralCharacters(false);
            return cpfFormat.valueToString(nuCpf.trim());
        } catch (ParseException e) {
            System.out.println("[ERROR] Exception while formatString " + e);
        }
        return null;
    }

    public static String getCnpjWithMask(String nuCnpj) {
        try {
            if (nuCnpj == null || nuCnpj.trim().length() == 0) {
                return "";
            }
            MaskFormatter format = new javax.swing.text.MaskFormatter("##.###.###/####-##");
            format.setValueContainsLiteralCharacters(false);
            return format.valueToString(nuCnpj.trim());
        } catch (ParseException e) {
            System.out.println("[ERROR] Exception while formatString " + e);
        }
        return null;
    }

    public static String getTelWithMask(String nuTelefone) {
        try {
            if (nuTelefone == null || nuTelefone.trim().length() == 0) {
                return "";
            }
            MaskFormatter format;
            if (nuTelefone.length() == 11) {
               format = new javax.swing.text.MaskFormatter("(##)#####-####");
            } else {
                format = new javax.swing.text.MaskFormatter("(##)####-####");
            }            format.setValueContainsLiteralCharacters(false);
            return format.valueToString(nuTelefone.trim());
        } catch (ParseException e) {
            System.out.println("[ERROR] Exception while formatString " + e);
        }
        return null;
    }

    public static String getCepWithMask(String nuCep) {
        try {
            if (nuCep == null || nuCep.trim().length() == 0) {
                return "";
            }
            MaskFormatter format = new javax.swing.text.MaskFormatter("#####-###");
            format.setValueContainsLiteralCharacters(false);
            return format.valueToString(nuCep.trim());
        } catch (ParseException e) {
            System.out.println("[ERROR] Exception while formatString " + e);
            return "";
        }
    }

    public static String removeCepCpfCpnjTelMask(String str) {
        if (str == null || str.trim().length() == 0) {
            return "";
        }
        return str.trim()
                .replaceAll(" ", "")
                .replaceAll("\\-", "")
                .replaceAll("\\.", "")
                .replaceAll("\\(", "")
                .replaceAll("\\)", "")
                .replaceAll("\\/", "");
    }


    public static String dtToBR(Timestamp dtTime) {
        if (dtTime == null) {
            return "";
        }

        String pattern = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(new Date(dtTime.getTime()));
    }

    public static String dtToBR(Date dtTime) {
        if (dtTime == null) {
            return "";
        }
        String pattern = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(dtTime);
    }

    public static String dtFullTimeToBR(Timestamp dtTime) {
        if (dtTime == null) {
            return "";
        }
        String pattern = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(new Date(dtTime.getTime()));
    }

    public static BigDecimal valorReaisBigDecimal(String valor) {
        if (valor == null || valor.isEmpty()) {
            return new BigDecimal(0);
        }
        valor = valor.replaceAll("\\.", "");
        valor = valor.replaceAll("\\,", ".");
        valor = valor.replaceAll(",", ".");

        return new BigDecimal(valor);
    }

    public static String removerMascara(String valor) {
        return valor.replaceAll("[^0-9]", "");
    }
}
