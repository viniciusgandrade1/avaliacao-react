--liquibase formatted sql

--changeset viniciusgandrade:1
-- Table: public.tb_perfil

-- DROP TABLE public.tb_perfil;

CREATE TABLE public.tb_perfil
(
    id_perfil bigserial NOT NULL,
    role character varying(12) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tb_perfil_pkey PRIMARY KEY (id_perfil)
)

TABLESPACE pg_default;

ALTER TABLE public.tb_perfil
    OWNER to postgres;

-- Table: public.tb_usuario

-- DROP TABLE public.tb_usuario;

CREATE TABLE public.tb_usuario
(
    id_usuario bigserial NOT NULL,
    username character varying(200) COLLATE pg_catalog."default" NOT NULL,
    password character varying(200) COLLATE pg_catalog."default" NOT NULL,
    id_perfil bigint NOT NULL,
    CONSTRAINT tb_usuario_pkey PRIMARY KEY (id_usuario),
    CONSTRAINT fk_perfil FOREIGN KEY (id_perfil)
        REFERENCES public.tb_perfil (id_perfil) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.tb_usuario
    OWNER to postgres;

-- Table: public.tb_endereco

-- DROP TABLE public.tb_endereco;

CREATE TABLE public.tb_endereco
(
    id_endereco bigserial NOT NULL,
    cep character varying COLLATE pg_catalog."default" NOT NULL,
    logradouro character varying COLLATE pg_catalog."default" NOT NULL,
    bairro character varying COLLATE pg_catalog."default" NOT NULL,
    cidade character varying COLLATE pg_catalog."default" NOT NULL,
    uf character varying COLLATE pg_catalog."default" NOT NULL,
    complemento character varying COLLATE pg_catalog."default",
    CONSTRAINT tb_endereco_pkey PRIMARY KEY (id_endereco)
)

TABLESPACE pg_default;

ALTER TABLE public.tb_endereco
    OWNER to postgres;

-- Table: public.tb_cliente

-- DROP TABLE public.tb_cliente

CREATE TABLE public.tb_cliente
(
    id_cliente bigserial NOT NULL,
    nome character varying COLLATE pg_catalog."default" NOT NULL,
    cpf character varying COLLATE pg_catalog."default" NOT NULL,
    id_endereco bigint NOT NULL,
    CONSTRAINT tb_person_pkey PRIMARY KEY (id_cliente),
    CONSTRAINT fk_endereco FOREIGN KEY (id_endereco)
        REFERENCES public.tb_endereco (id_endereco) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.tb_cliente
    OWNER to postgres;

-- Table: public.tb_email

-- DROP TABLE public.tb_email;

CREATE TABLE public.tb_email
(
    id_email bigserial NOT NULL,
    ds_email character varying COLLATE pg_catalog."default" NOT NULL,
    id_cliente bigint NOT NULL,
    CONSTRAINT tb_email_pkey PRIMARY KEY (id_email),
    CONSTRAINT fk_cliente_email FOREIGN KEY (id_cliente)
        REFERENCES public.tb_cliente (id_cliente) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.tb_email
    OWNER to postgres;

-- Table: public.tb_telefone

-- DROP TABLE public.tb_telefone;

CREATE TABLE public.tb_telefone
(
    id_telefone bigserial NOT NULL,
   numero character varying(9) COLLATE pg_catalog."default" NOT NULL,
   tipo character varying(11) COLLATE pg_catalog."default" NOT NULL,
   id_cliente bigint NOT NULL,
    CONSTRAINT tb_telefone_pkey PRIMARY KEY (id_telefone),
    CONSTRAINT fk_cliente_telefone FOREIGN KEY (id_cliente)
        REFERENCES public.tb_cliente (id_cliente) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.tb_telefone
    OWNER to postgres;
