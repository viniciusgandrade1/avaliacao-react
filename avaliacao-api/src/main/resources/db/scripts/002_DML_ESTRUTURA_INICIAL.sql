--liquibase formatted sql

--changeset viniciusgandrade:2

INSERT INTO public.tb_perfil(
    id_perfil, role)
    VALUES (1, 'ROLE_ADMIN');
INSERT INTO public.tb_perfil(
    id_perfil, role)
    VALUES (2, 'ROLE_DEFAULT');

INSERT INTO public.tb_usuario(
    id_usuario, username, password, id_perfil)
    VALUES (1, 'admin', '$2a$10$RRBAj8HqjFoNoAhguDFabuNQZEg4BmU8jwcgOikG6p9/inPeShZJS', 1);
INSERT INTO public.tb_usuario(
    id_usuario, username, password, id_perfil)
    VALUES (2, 'comum', '$2a$10$xdGK0w7sS.YwezgFSDHInuMr.3GQ66Z3x6Hp.SMxMP9MB7doS1mp2', 2);
