--liquibase formatted sql

--changeset viniciusgandrade:3

alter table public.tb_telefone alter column tipo type integer USING tipo::integer;
