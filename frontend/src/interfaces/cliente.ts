import { Endereco } from './endereco';
import { Telefone } from './telefone';
import { Email } from './email';

export interface Cliente {
  idCliente: number;
  nome: string;
  cpf: string;
  endereco: Endereco,
  emails: Array<Email>,
  telefones: Array<Telefone>
}
