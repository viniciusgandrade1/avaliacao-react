import { Cliente } from './cliente';

export interface Email {
  idEmail?: number;
  dsEmail: string;
  cliente?: Cliente;
}

