export interface Endereco {
  idEndereco: number;
  cep: string;
  bairro: string;
  logradouro: string;
  cidade: string;
  uf: string;
  complemento: string;
}
