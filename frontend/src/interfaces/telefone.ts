import { Cliente } from './cliente';

export interface Telefone {
  idTelefone: number;
  numero: string;
  tipo: number;
  cliente: Cliente;
}
