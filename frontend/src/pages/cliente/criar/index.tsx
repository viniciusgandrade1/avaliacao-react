import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Cliente } from '../../../interfaces/cliente';
import { Cep } from '../../../interfaces/cep';
import { useFormik, FormikProvider } from 'formik';
import * as Yup from 'yup';
import { Button, createStyles, TextField, TextFieldProps, Theme, Backdrop, CircularProgress } from '@material-ui/core';
import InputMask, {Props} from 'react-input-mask';
import { makeStyles } from '@material-ui/styles';
import api from '../../../services/api';
import './loading.scss';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { Endereco } from '../../../interfaces/endereco';
import { Email } from '../../../interfaces/email';
import { forkJoin, Observable, of, throwError } from 'rxjs';
import { ModalEmail } from './modals/modal-email';
import { FaTrash } from 'react-icons/fa';
import { Telefone } from '../../../interfaces/telefone';
import { ModalTelefone } from './modals/modal-telefone';

const CriarClientePage: React.FC = () => {
  let { id }: any = useParams();
  const [cliente, setCliente] = useState<Cliente>({} as Cliente);
  const [loading, setLoading] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isOpenTelefone, setIsOpenTelefone] = useState(false);
  const [mailList, setMailList] = useState<Array<Email>>([]);
  const [telefoneList, setTelefoneList] = useState<Array<Telefone>>([]);
  const history = useHistory();

  const formik = useFormik({
    initialValues: cliente,
    validationSchema: Yup.object({
      idCliente: Yup.number(),
      nome: Yup.string()
        .max(100, 'Nome deve conter menos de 100 caracteres')
        .min(3, 'Nome deve conter mais de 3 caracteres')
        // .matches(/^[\w\s]*$/, 'Nome deve conter apenas caracteres alfanuméricos')
        .required('Campo obrigatório'),
      cpf: Yup.string().required('Nome é obrigatório'),
      endereco: Yup.object({
        idEndereco: Yup.number(),
        cep: Yup.string().required('CEP é obrigatório'),
        logradouro: Yup.string().required('Logradouro é obrigatório'),
        bairro: Yup.string().required('Bairro é obrigatório'),
        cidade: Yup.string().required('Cidade é obrigatória'),
        uf: Yup.string().required('UF é obrigatório'),
        complemento: Yup.string()
      })
    }),
    onSubmit: (values: Cliente) => {
      if (mailList.length === 0 || telefoneList.length === 0) {
        return;
      }
      setLoading(true);
      values.endereco.cep = values.endereco.cep.replace(/\D/g, '');
      let obs = api.post<Endereco>('/endereco', values.endereco);
      if (values.endereco.idEndereco) {
        obs = api.put<Endereco>('/endereco/' + values.endereco.idEndereco, values.endereco);
      }
      let clienteCadatrado: Cliente = {} as Cliente;
      obs.pipe(
          mergeMap((endereco) => {
            const cliente: any = {
              nome: values.nome,
              cpf: values.cpf,
              endereco: {
                idEndereco: endereco.idEndereco
              }
            };
            let obs1 = api.post<Cliente>('/cliente', cliente);
            if (values.idCliente) {
              obs1 = api.put<Cliente>('/cliente/' + values.idCliente, cliente);
            }
            return obs1;
          }),
          mergeMap((cliente) => {
            clienteCadatrado = cliente;
            const fork: Array<Observable<Email>> = [];
            mailList.map((mail) => fork.push(api.post('/email', Object.assign({}, mail, {
              cliente: {
                idCliente: clienteCadatrado.idCliente
              }
            }))));
            return forkJoin(fork);
          }),
          mergeMap(() => {
            const fork: Array<Observable<Telefone>> = [];
            telefoneList.map((telefone) => fork.push(api.post('/telefone', Object.assign({}, telefone, {
              cliente: {
                idCliente: clienteCadatrado.idCliente
              }
            }))));
            return forkJoin(fork);
          }),
          catchError(error => {
            setLoading(false);
            return throwError(error);
          })
        ).subscribe(() => {
          setLoading(false);
          history.push('/');
      })
    },
  });

  useEffect(() => {
    if (id) {
      api.get<Cliente>('/cliente/' + id).subscribe((response) => {
        if (response) {
          setCliente(response);
          formik.setValues(response);
          setMailList(response.emails || []);
          setTelefoneList(response.telefones || []);
        }
      });
    }
  },[]);

  const onChangeCep = (event: any) => {
    const cep = event.target.value.replace(/\D/g, '');
    if (cep.length === 8) {
      setLoading(true);
      api.get<Cep>('/endereco/cep/' + cep).subscribe((data) => {
        if (data && Object.keys(data).length > 0) {
          formik.setFieldValue('endereco.bairro', data.bairro, true);
          formik.setFieldValue('endereco.logradouro', data.logradouro, true);
          formik.setFieldValue('endereco.complemento', data.complemento, true);
          formik.setFieldValue('endereco.cidade', data.localidade, true);
          formik.setFieldValue('endereco.uf', data.uf, true);
        }
        setLoading(false);
      })
    }
  };

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
      },
      info: {
        padding: '5px 10px',
        border: '1px solid blue',
        borderRadius: '5px',
        marginBottom: '5px',
        maxWidth: 300
      }
    }),
  );

  const classes: any = useStyles();

  const removerEmail = (id: number | undefined, index: number) => {
    mailList.splice(index, 1);
    if (id) {
      api.delete('/email', id).subscribe();
      setMailList(mailList);
    }
  };

  const removerTelefone = (id: number | undefined, index: number) => {
    telefoneList.splice(index, 1);
    if (id) {
      api.delete('/telefone', id).subscribe();
      console.log(telefoneList);
      setTelefoneList(telefoneList);
    }
  };

  return (
    <div style={{padding: 50}}>
      <FormikProvider value={formik}>
        <form onSubmit={formik.handleSubmit}>
          <TextField
            style={{margin: 10}}
            fullWidth
            id="nome"
            name="nome"
            label="Nome"
            value={formik.values.nome}
            onChange={formik.handleChange}
            error={formik.touched.nome && Boolean(formik.errors.nome)}
            helperText={formik.touched.nome && formik.errors.nome}
          />

          <InputMask
            mask="999.999.999-99"
            value={formik.values.cpf}
            onChange={formik.handleChange}
          >
            {(inputProps: Props & TextFieldProps)=>
              <TextField
                {...inputProps}
                type="tel"
                style={{margin: 10}}
                label="CPF"
                fullWidth
                id="cpf"
                name="cpf"
                error={formik.touched.cpf && Boolean(formik.errors.cpf)}
                helperText={formik.touched.cpf && formik.errors.cpf}
              />
            }
          </InputMask>
          <InputMask
            mask="99999-999"
            value={formik.values.endereco?.cep}
            onChange={formik.handleChange}
            onBlur={onChangeCep}
          >
            {(inputProps: Props & TextFieldProps)=>
              <TextField
                {...inputProps}
                type="tel"
                label="CEP"
                style={{margin: 10}}
                fullWidth
                id="endereco.cep"
                name="endereco.cep"
                error={formik.touched.endereco?.cep && Boolean(formik.errors.endereco?.cep)}
                helperText={formik.touched.endereco?.cep && formik.errors.endereco?.cep}
              />
            }
          </InputMask>
          <TextField
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            id="endereco.logradouro"
            name="endereco.logradouro"
            style={{margin: 10}}
            label="Logradouro"
            value={formik.values.endereco?.logradouro}
            onChange={formik.handleChange}
            error={formik.touched.endereco?.logradouro && Boolean(formik.errors.endereco?.logradouro)}
            helperText={formik.touched.endereco?.logradouro && formik.errors.endereco?.logradouro}
          />
          <TextField
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            style={{margin: 10}}
            id="endereco.bairro"
            name="endereco.bairro"
            label="Bairro"
            value={formik.values.endereco?.bairro}
            onChange={formik.handleChange}
            error={formik.touched.endereco?.bairro && Boolean(formik.errors.endereco?.bairro)}
            helperText={formik.touched.endereco?.bairro && formik.errors.endereco?.bairro}
          />
          <TextField
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            style={{margin: 10}}
            id="endereco.cidade"
            name="endereco.cidade"
            label="Cidade"
            value={formik.values.endereco?.cidade}
            onChange={formik.handleChange}
            error={formik.touched.endereco?.cidade && Boolean(formik.errors.endereco?.cidade)}
            helperText={formik.touched.endereco?.cidade && formik.errors.endereco?.cidade}
          />
          <TextField
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            style={{margin: 10}}
            id="endereco.uf"
            name="endereco.uf"
            label="UF"
            value={formik.values.endereco?.uf}
            onChange={formik.handleChange}
            error={formik.touched.endereco?.uf && Boolean(formik.errors.endereco?.uf)}
            helperText={formik.touched.endereco?.uf && formik.errors.endereco?.uf}
          />
          <TextField
            fullWidth
            style={{margin: 10}}
            InputLabelProps={{
              shrink: true,
            }}
            id="endereco.complemento"
            name="endereco.complemento"
            label="Complemento"
            value={formik.values.endereco?.complemento}
            onChange={formik.handleChange}
            error={formik.touched.endereco?.complemento && Boolean(formik.errors.endereco?.complemento)}
            helperText={formik.touched.endereco?.complemento && formik.errors.endereco?.complemento}
          />
          {
            mailList.map((email, index) => {
              return <div className={classes.info} style={{position: 'relative'}} key={index}>
                {email.dsEmail}
                <Button type="button" style={{position: 'absolute', right: '-10px'}}
                        onClick={() => removerEmail(email.idEmail, index)}><FaTrash/></Button>
              </div>
            })
          }
          <Button color="primary" type="button" variant="contained" onClick={() => {
            setIsOpen(true);
          }} style={{margin: 10}}>
            Adicionar email
          </Button>

          <ModalEmail isOpen={isOpen} confirm={(value: string) => {
            setIsOpen(false);
            mailList.push({
              dsEmail: value
            });
            setMailList(mailList);
          }}/>

          {
            telefoneList.map((telefone, index) => {
              return <div className={classes.info} style={{position: 'relative'}} key={index}>
                {telefone.numero} ({telefone.tipo === 1 ? 'Residencial' : telefone.tipo === 2 ? 'Comercial' : 'Celular'})
                <Button type="button" style={{position: 'absolute', right: '-10px'}}
                        onClick={() => removerTelefone(telefone.idTelefone, index)}><FaTrash/></Button>
              </div>
            })
          }
          <Button color="primary" type="button" variant="contained" onClick={() => {
            setIsOpenTelefone(true);
          }} style={{margin: 10}}>
            Adicionar telefone
          </Button>

          <ModalTelefone isOpen={isOpenTelefone} confirm={(value: Telefone) => {
            setIsOpenTelefone(false);
            telefoneList.push(value);
            setTelefoneList(telefoneList);
          }}/>

          <Button color="primary" variant="contained" fullWidth type="submit">
            Cadastrar
          </Button>
        </form>
      </FormikProvider>
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
};

export default CriarClientePage;
